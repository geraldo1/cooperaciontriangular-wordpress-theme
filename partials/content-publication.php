<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Go
 */

?>

<?php
	$own = "";
	if (get_field('own_publication')) $own = "own_pub";
?>

<div class="content-area__wrapper publication-wrapper">
	<div class="content-area entry-content">
		<!--<p id="volver"><a href="<?php echo get_site_url(); ?>/publicaciones">< Volver a publicaciones</a></p>-->

		<?php get_template_part( 'partials/pagination', 'post', array('location' => 'before', 'label' => 'Publicación') ); ?>
	</div>
</div>

<article <?php post_class($own); ?> id="post-<?php the_ID(); ?>">

	<div class="<?php Go\content_wrapper_class( 'content-area__wrapper' ); ?>">

		<div id="single-pub" class="content-area entry-content">

			<div class="wp-block-group">

				<div class="feature-img">

					<?php
			    		if ( has_post_thumbnail() ) {
							the_post_thumbnail("publication", ['class' => 'img-responsive responsive--full thumb-publication', 'title' => 'Feature image']);
						}
						else {
							echo '<img class="pub-placeholder" src="'.get_stylesheet_directory_uri().'/assets/placeholder-publicaciones.png" />';
						}

						if (get_field('own_publication')) {
							echo '<img class="icon-own-pub" src="'.get_stylesheet_directory_uri().'/assets/Sello-PublicacionesPropias.svg" />';
						}
					?>

				</div>

				<div class="content-text">

					<?php get_template_part( 'partials/content', 'publication-header' ); ?>

					<?php the_content(); ?>

				</div>

			</div>

		</div>

	</div>

</article>

<div class="content-area__wrapper">
	<div class="content-area entry-content">

		<?php get_template_part( 'partials/pagination', 'post', array('location' => 'after', 'label' => 'Publicación') ); ?>

	</div>
</div>
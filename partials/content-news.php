<?php 
	// the query
	$the_query = new WP_Query( array(
    	//'category_name' => 'news',
    	'posts_per_page' => $args['num'],
    	'post__not_in' => get_option( 'sticky_posts' ),
	)); 
?>

<?php if ( $the_query->have_posts() ) : $i=0; ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>

  	<article class="news">

	    <?php if ( (has_post_thumbnail() && ($i%2==0)) || wp_is_mobile() ) {
	    	echo "<div class='feature-img'>";
			the_post_thumbnail("thumbnail"/*, ['class' => $align, 'title' => 'Feature image']*/);
	    	echo "</div>";
		} ?>
		<div class="text-content">
		    <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		    <p class="date">
		    	<?php echo get_the_date('d / m / Y'); ?>
		    </p>
		    <p><?php echo excerpt(45); ?></p>
		</div>
	    <?php if ( has_post_thumbnail() && ($i%2!=0) && !wp_is_mobile() ) {
	    	echo "<div class='feature-img align-right'>";
			the_post_thumbnail("thumbnail"/*, ['class' => $align, 'title' => 'Feature image']*/);
	    	echo "</div>";
		} ?>
	</article>

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>

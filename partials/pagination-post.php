<div class="nav-<?php echo $args['location']; ?>">

<?php

	the_post_navigation(
		array(
			'prev_text' => '<div class="alignleft"><span class="screen-reader-text">%title</span><img class="icon" src="' . get_stylesheet_directory_uri() . '/assets/anterior-flecha.png" /><span class="nav-title">' . __('Publicación anterior', 'go-child' ) . '</span></div>',
			'next_text' => '<div class="alignright"><span class="screen-reader-text"> %title</span><span class="nav-title">' . __('Publicación posterior', 'go-child' ) . '</span><img class="icon" src="' . get_stylesheet_directory_uri() . '/assets/siguente-flecha.png" /></div>',
		)
	);

?>

<script>
var html = `
<div class="nav-up">
	<a href="<?php echo site_url(); ?>/<?php if ($args['label'] == 'Noticia') echo "noticias"; else echo "publicaciones"; ?>/" rel="next">
		<div class="aligncenter">
			<span class="screen-reader-text">Arriba: Mostrar todas las <?php echo $args['label']; ?></span><img class="icon-up" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-noticias-arriba.png">
		</div>
	</a>
</div>
`;

jQuery( document ).ready(function($) {
	$(".nav-<?php echo $args['location']; ?> .nav-links").append(html);
});
</script>

</div>


<?php 
	// the query
	$the_query = new WP_Query( array(
    	//'category_name' => 'news',
    	'posts_per_page' => 1,
    	'post__in' => get_option( 'sticky_posts' ),
    	'ignore_sticky_posts' => 1,
	)); 
?>

<?php if ( $the_query->have_posts() && isset(get_option( 'sticky_posts' )[0]) ) : ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  	<article class="news-sticky">
	    <?php if ( has_post_thumbnail() ) {
	    	$bgImg = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
	    	echo "<div class='feature-img' style='background:url(".$bgImg[0].") no-repeat; background-size:cover;'>";
			//the_post_thumbnail("big"/*, ['class' => $align, 'title' => 'Feature image']*/);
	    	echo "</div>";
		} ?>
		<div class="text-content">
		    <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		    <p class="date">
		    	<?php the_date('d / m / Y'); ?>
		    </p>
		    <p><?php echo excerpt(0); ?></p>
		</div>
	</article>

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>

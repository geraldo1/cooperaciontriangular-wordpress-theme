<div class="content-area__wrapper">
	<div class="content-area entry-content">

		<?php 
		// wp-query to get all published posts without pagination
		$args = array(
			'post_type'		=> 'publication', 
			'post_status'	=> 'publish', 
			'posts_per_page'=> -1,
			'meta_query' 	=> array(
				array(
		            'key'	=> 'own_publication',
		            'value'	=> 1,
		        ),
		    )
		);

		$allPostsWPQuery = new WP_Query($args); 

		$lang = ICL_LANGUAGE_CODE;

		if ($lang == 'es') $lang = '';

		?>

		<?php if ( $allPostsWPQuery->have_posts() ) : ?>

			<a class="button btn-own-pub alignright" href="<?php echo get_site_url() . $lang; ?>/publicaciones/publicaciones-propias"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Own publications'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Publicaçãos próprias'; else echo 'Publicaciones propias'; ?></a>

		<?php endif; ?>

		<div id="swiper-own-pub" class="swiper-container">
			<div class="wp-swiper__wrapper swiper-wrapper">
				<?php if ( $allPostsWPQuery->have_posts() ) : ?>
				 
			    	<?php while ( $allPostsWPQuery->have_posts() ) : $allPostsWPQuery->the_post(); ?>

						<div class="wp-swiper__slide swiper-slide">
							<div class="wp-swiper__slide-content">

								<div class="wp-block-group article <?php if (get_field('own_publication')) echo 'own_pub'; ?>">

									<div class="wp-block-group__inner-container">

										<div class="wp-block-columns">
											<div class="wp-block-column feature-img">
												<?php
									    		if ( has_post_thumbnail() ) {
													the_post_thumbnail("publication", ['class' => 'img-responsive responsive--full thumb-publication', 'title' => 'Feature image']);
												}
												else {
													echo '<img class="pub-placeholder" src="'.get_stylesheet_directory_uri().'/assets/placeholder-publicaciones.png" />';
												}
												echo '<img class="icon-own-pub" src="'.get_stylesheet_directory_uri().'/assets/Sello-PublicacionesPropias.svg" />';
												?>
											</div>
											<div class="wp-block-column content-resume">
												<?php get_template_part( 'partials/content' , 'publication-header' ); ?>

									    	</div>
											<div class="wp-block-column content-text">
											    <?php echo excerpt(60); ?>
					    			    	</div>
					    			    </div>
									</div>
								</div>
							</div>
						</div>
			    	<?php endwhile; ?>

				     <?php wp_reset_postdata(); ?>
				<?php endif; ?>

			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		</div>
	</div>
</div>

<?php
wp_enqueue_script( 'swiper-init-js', get_stylesheet_directory_uri() . '/dist/js/swiper.js', array(), '1.0', true );
?>
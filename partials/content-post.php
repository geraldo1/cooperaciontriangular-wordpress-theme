<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Go
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="<?php Go\content_wrapper_class( 'content-area__wrapper' ); ?>">

		<div class="content-area entry-content">

			<?php get_template_part( 'partials/pagination', 'post', array('location' => 'before', 'label' => 'Noticia') ); ?>

			<div class="wp-block-group">

				<div class="wp-block-group__inner-container">

					<div class="post-header">
						<?php
				    		if ( has_post_thumbnail() ) {
								the_post_thumbnail("thumbnail", ['class' => 'img-responsive responsive--full thumb-news', 'title' => 'Feature image']);
							}
						?>

						<div class="title"><?php the_title(); ?></div>
						<p class="date">
					    	<?php echo get_the_date('d / m / Y'); ?>
					    </p>
					</div>

					<div class="post-content">
					
						<?php the_content(); ?>

					</div>

				</div>

			</div>

			<?php get_template_part( 'partials/pagination', 'post', array('location' => 'after', 'label' => 'Noticia', 'label' => 'Noticia') ); ?>

		</div>

	</div>

</article>

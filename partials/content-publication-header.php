<div class="super">
	<?php 
		/*if (get_field('own_publication')) {
			echo "<span class='own'>";
			if (ICL_LANGUAGE_CODE == 'en') echo 'Own publications'; 
				else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Publicaçãos próprias'; 
				else echo 'Publicaciones propias';
			echo "</span>";
		}*/

		echo "<span>".get_field('type_of_document')->name."</span>";
	?> &gt;
</div>
<!--<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>-->
<div class="title"><?php the_title(); ?></div>
<div class="date"><b><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Date of publication'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Data de publicação'; else echo 'Fecha de publicación'; ?>: </b> 
	<?php 
		$date = get_field('date_of_publicaction'); 
		$date = DateTime::createFromFormat('Ymd', $date);
		//echo ucfirst(__($date->format('F'))) . ' ' . $date->format('Y');
		echo $date->format('Y');
	?>
</div>
<div class="authors"><b><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Authors'; else echo 'Autor/es'; ?>: </b> 
	<?php 
		$authors = get_field('author');
		foreach ($authors as $i => $author) {
			if ($i > 0) echo "; ";
			echo $author->name;
		}
	?>
</div>
<div class="entities"><b><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Publishing entity'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Entidade editora'; else echo 'Entidad que publica'; ?>: </b> 
	<?php 
		$entities = get_field('entity');
		if ($entities) {
			foreach ($entities as $i => $entity) {
				if ($i > 0) echo ", ";
				echo $entity->name;
			}
		}
	?>
</div>

<?php if (get_field('archivo_o_url_externa') == 'Archivo') : ?>

	<div class="download"><a class="button" target="_blank" href="<?php the_field('archivo'); ?>"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Download'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Baixar'; else echo 'Descarga'; ?></a></div>

<?php else: ?>

	<div class="download"><a class="button" target="_blank" href="<?php the_field('url'); ?>"><?php echo __(get_field('titulo_url'), "go-child"); ?></a></div>


<?php endif; ?>
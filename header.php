<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="site-content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Go
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-E47PXQVDR3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-E47PXQVDR3');
	</script>

	<?php wp_head(); ?>
</head>

<body
	<?php
	$body_class = get_body_class();
	if ( Go\AMP\is_amp() ) {
		?>
		aria-expanded="false"
		[aria-expanded]="mainNavMenuExpanded ? 'true' : 'false'"
		[class]="'<?php echo esc_attr( implode( ' ', $body_class ) ); ?>' + ( mainNavMenuExpanded ? ' menu-is-open' : '' )"
		<?php
	}
	?>
	class="<?php echo esc_attr( implode( ' ', $body_class ) ); ?>"
>

	<?php wp_body_open(); ?>

	<div id="page" class="site">

		<a class="skip-link screen-reader-text" href="#site-content"><?php esc_html_e( 'Skip to content', 'go' ); ?></a>

		<header id="site-header" class="site-header header relative <?php echo esc_attr( Go\has_header_background() ); ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader">

			<div class="header__inner flex items-center justify-between h-inherit w-full relative">

				<div class="header-logos">
					<a target="_blank" href="https://www.segib.org/"><img class="segib" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo-segib-ES.svg" /></a>
					<a target="_blank" href="https://ec.europa.eu/"><img class="eu" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo-UE-<?php echo strtoupper(ICL_LANGUAGE_CODE); ?>.svg" /></a>
				</div>

				<div class="header__extras">
					<?php //Go\search_toggle(); ?>

					<form class="navbar-form" role="search" method="get" id="searchform" action="<?php bloginfo('home'); ?>" >
						<div class="input-group">
							<label for="form-control" class="buscar-label">
								<?php 
									if (ICL_LANGUAGE_CODE == 'en') echo 'Search';
									else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Procurar';
									else echo 'Buscar';
								?>:
							</label>
							<input type="text" id="searchbox" class="form-control" name="s" id="s" />
							<input type="image" alt="Search" class="search-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/search.svg" />
							<!--<div class="header__search-toggle">
								<svg role="img" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="m18.0553691 9.08577774c0-4.92630404-4.02005-8.94635404-8.94635408-8.94635404-4.92630404 0-8.96959132 4.02005-8.96959132 8.94635404 0 4.92630406 4.02005 8.94635406 8.94635404 8.94635406 2.13783006 0 4.08976186-.7435931 5.64665986-1.9984064l3.8109144 3.8109145 1.3245252-1.3245252-3.8341518-3.7876771c1.2548133-1.5336607 2.0216437-3.5088298 2.0216437-5.64665986zm-8.96959136 7.11060866c-3.90386358 0-7.08737138-3.1835078-7.08737138-7.08737138s3.1835078-7.08737138 7.08737138-7.08737138c3.90386356 0 7.08737136 3.1835078 7.08737136 7.08737138s-3.1602705 7.08737138-7.08737136 7.08737138z"></path></svg>
							</div>-->
						</div>
					</form>

					<?php do_action('wpml_add_language_selector'); ?>
				</div>

			</div>

			<div class="second header__inner flex items-center justify-between h-inherit relative">

				<div class="header__title-nav flex items-center flex-nowrap">

					<?php //Go\display_site_branding(); ?>

					<div class="header__titles lg:flex items-center" itemscope="" itemtype="http://schema.org/Organization">

						<a href="<?php echo get_home_url(); ?>" class="custom-logo-link" rel="home" aria-current="page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Logo-Proyecto-<?php echo strtoupper(ICL_LANGUAGE_CODE); ?>.svg" class="custom-logo" alt=""></a>

					</div>

					<?php if ( has_nav_menu( 'primary' ) ) : ?>

						<nav id="header__navigation" class="header__navigation" aria-label="<?php esc_attr_e( 'Horizontal', 'go' ); ?>" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

							<div class="header__navigation-inner">
								<?php
								wp_nav_menu(
									array(
										'menu_class'     => 'primary-menu list-reset',
										'theme_location' => 'primary',
									)
								);
								?>
							</div>

						</nav>

					<?php endif; ?>

				</div>

				<?php Go\navigation_toggle(); ?>

			</div>

			<?php get_template_part( 'partials/modal-search' ); ?>

		</header>

		<main id="site-content" class="site-content" role="main">

			<div class="content-area__wrapper">
				<div class="content-area entry-content">

					<?php if (!is_front_page()) : ?>

						<p id="breadcrumb"><?php bcn_display(); ?></p>

					<?php endif; ?>

				</div>
			</div>
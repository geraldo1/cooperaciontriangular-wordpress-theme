<?php
/**
 * Template Name: Frontpage template
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Go
 */

get_header();
?>

<?php
// Start the Loop.
while ( have_posts() ) :
	the_post();
	get_template_part( 'partials/content', 'page' );
endwhile;
?>

<?php get_template_part( 'partials/content-publication-slider' ); ?>

<!--<div class="content-area__wrapper section-news">
	<div class="content-area entry-content">

		<?php //get_template_part( 'partials/content', 'news-sticky' ); ?>

		<?php //get_template_part( 'partials/content', 'news', array('num' => 2) ); ?>

		<p class="more-news"><a class="button" href="<?php //echo get_site_url() . '/' . ICL_LANGUAGE_CODE; ?>/noticias"><?php //if (ICL_LANGUAGE_CODE == 'en') echo 'More news'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Mais notícias'; else echo 'Más noticias'; ?></a></p>

	</div>
</div>-->

<div class="front-footer">

	<div class="ods aligncenter">
		<a target="_blank" href="https://www.undp.org/content/undp/es/home/sustainable-development-goals.html">
			<div class="ods-text">
				<?php 
					//$copyright = get_theme_mod( 'copyright', \Go\Core\get_default_copyright() );
					//if ($copyright) echo $copyright;
				if (ICL_LANGUAGE_CODE === "pt-br")
					echo "A SEGIB está a assumir um compromisso decisivo com a Cooperação Triangular como um mecanismo eficaz para a implementação da Agenda 2030.";
				else
					echo __("ods_text", "go-child");
				?>
			</div>
			<div class="ods-image ods-image-<?php echo ICL_LANGUAGE_CODE; ?>"></div>
		</a>
	</div>

</div>

<?php

wp_enqueue_script( 'front-js', get_stylesheet_directory_uri() . '/dist/js/front.js', array(), '1.0', true );

get_footer();

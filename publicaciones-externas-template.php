<?php
/**
 * Template Name: Publicaciones externas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Go
 */

get_header();

// Start the Loop.
while ( have_posts() ) :
	the_post();
	get_template_part( 'partials/content', 'page' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}

endwhile;
?>

<?php
	// search
	$isSearch = false;

	if ((isset($_GET["search_pub"]) && $_GET["search_pub"] != "") ||
		(isset($_GET["search_year"]) && $_GET["search_year"] != "") ||
		(isset($_GET["search_author"]) && $_GET["search_author"] != "") ||
		(isset($_GET["search_entity"]) && $_GET["search_entity"] != "") ||
		(isset($_GET["search_lang"]) && $_GET["search_lang"] != "")) {

		$isSearch = true;
	}
?>

<div class="content-area__wrapper publications">
	<div class="content-area entry-content">

	<?php if ($isSearch) {
		echo '<div class="filtros">Filtros busqueda:';
	} ?>

	<?php 
	// wp-query to get all published posts without pagination
	$args = array(
		'post_type'		=> 'publication', 
		'post_status'	=> 'publish', 
		'posts_per_page'=> -1,
		'meta_key'		=> 'date_of_publicaction',
		'orderby'		=> 'meta_value',
		'order'			=> 'DESC',
		'suppress_filters' => true
	);
	$active_lang = ICL_LANGUAGE_CODE;
	$lang_name = ['es' => 'Español', 'en' => 'English', 'pt-br' => 'Português'];

	if ($isSearch) {
		$meta_query = array(
			array(
	            'key'	=> 'own_publication',
	            'value'	=> 0,
	        ),
		);

		if ($_GET["search_pub"] != "") {
			$pub = $_GET["search_pub"];

			$meta_query[] = array(
				array(
		            'key'	=> 'type_of_document',
		            'value'	=> $pub,
		        ),
			);
			echo '<span class="filtro">'.get_term_by('id', $pub, 'publication_category')->name.'</span>';

			echo ' <span class="remove-search" data-type="search_pub">x</span>';
		}

		if ($_GET["search_year"] != "") {
			$year = $_GET["search_year"];
			$meta_query[] = array(
				array(
		            'key'	=> 'date_of_publicaction',
		            'value'	=> array($year.'-01-01', $year.'-12-31'),
		            'type'	=> 'DATE',
		            'compare' => 'BETWEEN',
		        ),
			);

			echo '<span class="filtro">'.$year.'</span>';
			echo ' <span class="remove-search" data-type="search_year">x</span>';
		}

		if ($_GET["search_author"] != "") {
			$author = $_GET["search_author"];
			$meta_query[] = array(
				array(
		            'key'	=> 'author',
		            'value'	=> $author,
		            'compare' => 'LIKE',
		        ),
			);

			echo '<span class="filtro">'.get_term_by('id', $author, 'author')->name.'</span>';
			echo ' <span class="remove-search" data-type="search_author">x</span>';
		}

		if ($_GET["search_entity"] != "") {
			$entity = $_GET["search_entity"];
			$meta_query[] = array(
				array(
		            'key'	=> 'entity',
		            'value'	=> $entity,
		            'compare' => 'LIKE',
		        ),
			);

			echo '<span class="filtro">'.get_term_by('id', $entity, 'entity')->name.'</span>';
			echo ' <span class="remove-search" data-type="search_entity">x</span>';
		}

		if ($_GET["search_lang"] != "") {
			$lang = $_GET["search_lang"];
			$args['suppress_filters'] = false;

			// hook to Control language in WordPress queries
			// see https://wpml.org/wpml-hook/wpml_switch_language/
			do_action( 'wpml_switch_language', $lang );

			echo '<span class="filtro">'.$lang_name[$lang].'</span>';
			echo ' <span class="remove-search" data-type="search_lang">x</span>';
		}

		$args['meta_query']	= $meta_query;

		echo '</div>';
	}
	else {
		$args['meta_query']	= array(
			array(
	            'key'	=> 'own_publication',
	            'value'	=> 0,
	        ),
	    );
	}

	//trigger_error(json_encode($args), E_USER_WARNING);

	$allPostsWPQuery = new WP_Query($args); 

	do_action( 'wpml_switch_language', $active_lang );

	?>
	 
	<?php if ( $allPostsWPQuery->have_posts() ) : ?>
	 
    	<?php while ( $allPostsWPQuery->have_posts() ) : $allPostsWPQuery->the_post(); ?>

			<div class="wp-block-group article <?php if (get_field('own_publication')) echo 'own_pub'; ?>">

				<div class="wp-block-group__inner-container">

					<div class="wp-block-column feature-img pub-img">
						<?php
			    		if ( has_post_thumbnail() ) {
							the_post_thumbnail("publication-size", ['class' => 'thumb-publication', 'title' => 'Feature image']);
						}
						else {
							echo '<img class="pub-placeholder" src="'.get_stylesheet_directory_uri().'/assets/placeholder-publicaciones.png" />';
						}

						if (get_field('own_publication')) {
							echo '<img class="icon-own-pub" src="'.get_stylesheet_directory_uri().'/assets/Sello-PublicacionesPropias.svg" />';
						}
						?>
					</div>

					<div class="wp-block-columns pub-columns">
						<div class="wp-block-column content-resume">
							<?php get_template_part( 'partials/content', 'publication-header' ); ?>
				    	</div>
						<div class="wp-block-column content-text">
							<p><?php echo excerpt(50); ?></p>
    			    	</div>
    			    </div>
				</div>
			</div>
    	<?php endwhile; ?>

	     <?php wp_reset_postdata(); ?>
	 <?php else : ?>
	    <p>
	      	<?php
				if (ICL_LANGUAGE_CODE == 'en') echo 'No content to show'; 
				else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Nenhum conteúdo a ser exibido'; 
				else echo 'No hay contenidos para mostrar.';
 			?>
		</p>
	<?php endif; ?>

	</div>
</div>

<script>
	// add search
	var html = `
<div id="searchForm">
	<div class="inputs">
		<p>
			<span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Icons-tipo-publicacion.png" /></span>
			<label for="search_pub"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Type of publication'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Tipo de publicação'; else echo 'Tipo de publicación'; ?></label>
			<span class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-abajo.png"></span>
			<div class="custom-select-wrapper" id="search_pub" name="search_pub">
				<div class="custom-select">
			        <div class="custom-select__trigger">
			        	<span></span>
			        </div>
			        <div class="custom-options">
			            <span class="custom-option first selected" data-value=""></span>
						<?php
							$terms = get_terms( array(
							    'taxonomy' => 'publication_category',
							    'hide_empty' => false
							) );

						    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
						        foreach ( $terms as $term ) {
						            echo '<span class="custom-option' . (isset($_GET["search_pub"]) && $_GET["search_pub"] != "" && $_GET["search_pub"] == $term->term_id ? ' selected' : '') . '" data-value="' . $term->term_id . '">' . $term->name . '</span>';
						        }
						    }
						?>
			        </div>
			    </div>
			</div>
		</p>
		<p>
			<span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Icons-anyo.png" /></span>
			<label for="search_year"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Year of publication'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Ano de publicação'; else echo 'Año de publicación'; ?></label>
			<span class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-abajo.png"></span>
			<div class="custom-select-wrapper" id="search_year" name="search_year">
				<div class="custom-select">
			        <div class="custom-select__trigger">
			        	<span></span>
			        </div>
			        <div class="custom-options">
			            <span class="custom-option first selected" data-value=""></span>
						<?php 
							$args = array(
								'post_type'		=> 'publication', 
								'post_status'	=> 'publish', 
								'posts_per_page'=> -1,
							);
							$allPostsWPQuery = new WP_Query($args);

							$years = array();

							if ( $allPostsWPQuery->have_posts() ) {
								while ( $allPostsWPQuery->have_posts() ) {
									$allPostsWPQuery->the_post();
									$date = get_field('date_of_publicaction');
									$date = DateTime::createFromFormat('Ymd', $date);
									$year = $date->format('Y');
									if (!in_array($year, $years)) {
										array_push($years, $year);
									}
						 			wp_reset_postdata();
								}
							}

							sort($years);

							for ($i=0; $i < sizeof($years); $i++) {
						        echo '<span class="custom-option' . (isset($_GET["search_year"]) && $_GET["search_year"] != "" && $_GET["search_year"] == $years[$i] ? ' selected' : '') . '" data-value="' . $years[$i] . '">' . $years[$i] . '</span>';
							}
						?>
			        </div>
			    </div>
			</div>
		</p>
		<p>
			<span class="icon autor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Icons-autores.png" /></span>
			<label for="search_author"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Authors'; else echo 'Autor/es'; ?></label>
			<span class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-abajo.png"></span>
			<div class="custom-select-wrapper" id="search_author" name="search_author">
				<div class="custom-select">
			        <div class="custom-select__trigger">
			        	<span></span>
			        </div>
			        <div class="custom-options">
			            <span class="custom-option first selected" data-value=""></span>
						<?php
							$terms = get_terms( array(
							    'taxonomy' => 'author',
							    'hide_empty' => false
							) );

						    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
						        foreach ( $terms as $term ) {
						            echo '<span class="custom-option' . (isset($_GET["search_author"]) && $_GET["search_author"] != "" && $_GET["search_author"] == $term->term_id ? ' selected' : '') . '" data-value="' . $term->term_id . '">' . $term->name . '</span>';
						        }
						    }
						?>
			        </div>
			    </div>
			</div>
		</p>
		<p>
			<span class="icon entity"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Icons-entidad.png" /></span>
			<label for="search_entity"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Entities'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Entitade/s'; else echo 'Entitad/es'; ?></label>
			<span class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-abajo.png"></span>
			<div class="custom-select-wrapper" id="search_entity" name="search_entity">
				<div class="custom-select">
			        <div class="custom-select__trigger">
			        	<span></span>
			        </div>
			        <div class="custom-options">
			            <span class="custom-option first selected" data-value=""></span>
						<?php
							$terms = get_terms( array(
							    'taxonomy' => 'entity',
							    'hide_empty' => false
							) );

						    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
						        foreach ( $terms as $term ) {
						            echo '<span class="custom-option' . (isset($_GET["search_entity"]) && $_GET["search_entity"] != "" && $_GET["search_entity"] == $term->term_id ? ' selected' : '') . '" data-value="' . $term->term_id . '">' . $term->name . '</span>';
						        }
						    }
						?>
			        </div>
			    </div>
			</div>
		</p>
		<p>
			<span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/Icons-idioma.png" /></span>
			<label for="search_lang"><?php if (ICL_LANGUAGE_CODE == 'en') echo 'Language'; else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Idioma'; else echo 'Idioma'; ?></label>
			<span class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/flecha-abajo.png"></span>
			<div class="custom-select-wrapper" id="search_lang" name="search_lang">
				<div class="custom-select">
			        <div class="custom-select__trigger">
			        	<span></span>
			        </div>
			        <div class="custom-options">
			            <span class="custom-option first selected" data-value=""></span>
						<?php
							$langs = ['es', 'en', 'pt-br'];
					        foreach ( $langs as $lang ) {
					            echo '<span class="custom-option' . (isset($_GET["search_lang"]) && $_GET["search_lang"] != "" && $_GET["search_lang"] == $lang ? ' selected' : '') . '" data-value="' . $lang . '">' . $lang_name[$lang] . '</span>';
					        }
						?>
			        </div>
			    </div>
			</div>
		</p>
	</div>
	<div class="btns">
		<button class="search-btn">
			<span>
				<?php 
					if (ICL_LANGUAGE_CODE == 'en') echo 'Search';
					else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Procurar';
					else echo 'Buscar';
				?>
			</span> 
			<!--<img class="search-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/lupe-publicaciones.png" />-->
			<svg role="img" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="m18.0553691 9.08577774c0-4.92630404-4.02005-8.94635404-8.94635408-8.94635404-4.92630404 0-8.96959132 4.02005-8.96959132 8.94635404 0 4.92630406 4.02005 8.94635406 8.94635404 8.94635406 2.13783006 0 4.08976186-.7435931 5.64665986-1.9984064l3.8109144 3.8109145 1.3245252-1.3245252-3.8341518-3.7876771c1.2548133-1.5336607 2.0216437-3.5088298 2.0216437-5.64665986zm-8.96959136 7.11060866c-3.90386358 0-7.08737138-3.1835078-7.08737138-7.08737138s3.1835078-7.08737138 7.08737138-7.08737138c3.90386356 0 7.08737136 3.1835078 7.08737136 7.08737138s-3.1602705 7.08737138-7.08737136 7.08737138z"></path></svg>
		</button>
	</div>
</div>
	`;

	jQuery( document ).ready(function($) {
		$(".buscador").append(html);
	});
</script>

<?php

wp_enqueue_script( 'pub-js', get_stylesheet_directory_uri() . '/dist/js/pub.js', array(), '1.0', true );
wp_enqueue_script( 'download-js', get_stylesheet_directory_uri() . '/dist/js/download.js', array(), '1.0', true );

get_footer();

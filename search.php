<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Go
 */

get_header();
?>

<div class="content-area__wrapper">
	<div class="content-area entry-content">

	<?php if ( have_posts() ) {

		Go\page_title();

		// Start the Loop.
		while ( have_posts() ) :
			the_post();
			get_template_part( 'partials/content', 'search' );
		endwhile;

		// Previous/next page navigation.
		get_template_part( 'partials/pagination' );

	} else {

		// If no content, include the "No posts found" template.
		get_template_part( 'partials/content', 'none' );
	}
	?>

	</div>
</div>

<?php
get_footer();

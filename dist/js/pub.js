jQuery( document ).ready(function($) {

	// search reset button
	$(".search-btn").click(function() {
		console.log("search");
		setUrl(
			$("#search_pub .custom-option.selected").data("value"),
			$("#search_year .custom-option.selected").data("value"),
			$("#search_author .custom-option.selected").data("value"),
			$("#search_entity .custom-option.selected").data("value"),
			$("#search_tag .custom-option.selected").data("value"),
			$("#search_lang .custom-option.selected").data("value")
		);
	});

	// filter remove button
	$(".remove-search").click(function() {
		$("#"+$(this).data("type")).val("");
		$(".search-btn").click();
	});

	// search form custom select
	for (const dropdown of document.querySelectorAll(".custom-select-wrapper")) {
	    dropdown.addEventListener('click', function() {
	        this.querySelector('.custom-select').classList.toggle('open');
	    })
	}

	for (const option of document.querySelectorAll(".custom-option")) {
	    option.addEventListener('click', function() {
	        if (!this.classList.contains('selected')) {
	            this.parentNode.querySelector('.custom-option.selected').classList.remove('selected');
	            this.classList.add('selected');
	            this.closest('.custom-select').querySelector('.custom-select__trigger span').textContent = this.textContent;
	        }
	    })
	}

	window.addEventListener('click', function(e) {
	    for (const select of document.querySelectorAll('.custom-select')) {
	        if (!select.contains(e.target)) {
	            select.classList.remove('open');
	        }
	    }
	});
});

function setUrl(pub, year, author, entity, tag, lang) {

	let baseurl = window.location.protocol.toString() + "//" + window.location.host.toString(),
		pubdir = "/publicaciones",
		params = "/?search_pub=" + pub + "&search_year=" + year + "&search_author=" + author + "&search_entity=" + entity + "&search_tag=" + tag + "&search_lang=" + lang;

	document.location.href = baseurl + pubdir + params;
}
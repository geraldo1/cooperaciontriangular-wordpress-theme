jQuery( document ).ready(function($) {

	if (window.wpSwiper.length > 0) {

		stopOnHover(window.wpSwiper[0]);
	}
});

function stopOnHover(swiper) {
    jQuery(".swiper-container").hover(function() {
        swiper.autoplay.stop();
    }, function() {
        swiper.autoplay.start();
    });
}

jQuery( document ).ready(function($) {

	if ($('#swiper-own-pub').length) {

		//$('#swiper-wrapper-own-pub').css('display', 'block');

		initSwiper('#swiper-own-pub');
	}
});

function initSwiper(swiperClass, swiperInitFunction=null) {
	return new Swiper(swiperClass, {
		loop: false,
		autoplay: true,
		speed: 500,

		pagination: {
		    el: '.swiper-pagination',
		    clickable: true,
		},

		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},

		on: {
		    init: function() {
			    jQuery(swiperClass).each(function(elem, target){
			    	var swp = target.swiper;
				    jQuery(this).hover(function() {
				        swp.autoplay.stop();
				    }, function() {
				        swp.autoplay.start();
				    });
				});
			}
		},
	});
}

<?php
/**
 * Template Name: Publicaciones propias template
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Go
 */

get_header();

// Start the Loop.
while ( have_posts() ) :
	the_post();
	get_template_part( 'partials/content', 'page' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}

endwhile;
?>

<div class="content-area__wrapper">
	<div class="content-area entry-content">

	<?php 
	// wp-query to get all published posts without pagination
	$args = array(
		'post_type'		=> 'publication', 
		'post_status'	=> 'publish', 
		'posts_per_page'=> -1,
		'meta_query' 	=> array(
			array(
	            'key'	=> 'own_publication',
	            'value'	=> 1,
	        ),
	    )
	);

	$allPostsWPQuery = new WP_Query($args); ?>
	 
	<?php if ( $allPostsWPQuery->have_posts() ) : ?>
	 
    	<?php while ( $allPostsWPQuery->have_posts() ) : $allPostsWPQuery->the_post(); ?>

			<div class="wp-block-group article own_pub">

				<div class="wp-block-group__inner-container">

					<div class="wp-block-columns">
						<div class="wp-block-column feature-img">
							<?php
				    		if ( has_post_thumbnail() ) {
								the_post_thumbnail("publication", ['class' => 'img-responsive responsive--full thumb-publication', 'title' => 'Feature image']);
							}
							else {
								echo '<img class="pub-placeholder" src="'.get_stylesheet_directory_uri().'/assets/placeholder-publicaciones.png" />';
							}
							?>
						</div>
						<div class="wp-block-column content-resume">
							<?php get_template_part( 'partials/content', 'publication-header' ); ?>
				    	</div>
						<div class="wp-block-column content-text">
						    <?php echo excerpt(30); ?>
    			    	</div>
    			    </div>
				</div>
			</div>
    	<?php endwhile; ?>

	    <?php wp_reset_postdata(); ?>
	 <?php else : ?>
	    <p>
	    	<?php
				if (ICL_LANGUAGE_CODE == 'en') echo 'No content to show'; 
				else if (ICL_LANGUAGE_CODE == 'pt-br') echo 'Nenhum conteúdo a ser exibido'; 
				else echo 'No hay contenidos para mostrar.';
 			?>
	    </p>
	<?php endif; ?>

	</div>
</div>

<?php

wp_enqueue_script( 'download-js', get_stylesheet_directory_uri() . '/dist/js/download.js', array(), '1.0', true );

get_footer();

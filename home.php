<?php
/**
 * Display all posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Go
 */

get_header();

?>

<div class="content-area__wrapper">
	<div class="content-area entry-content">

		<?php get_template_part( 'partials/content', 'news-sticky' ); ?>

		<?php get_template_part( 'partials/content', 'news', array('num' => 10) ); ?>

	</div>
</div>

<?php

get_footer();

<?php
/**
 * The template for displaying the footer.
 *
 * @package Go
 */

?>

	</main>

	<footer id="footer" class="px">

		<?php 
			$id = 849;
			if (ICL_LANGUAGE_CODE == 'en') $id = 1073;
			else if (ICL_LANGUAGE_CODE == 'pt-br') $id = 1075;
			$post   = get_post( $id );
			echo apply_filters( 'the_content', $post->post_content );
		?>

	</footer>

	<?php Go\footer_variation(); ?>

	</div>

	<?php wp_footer(); ?>

	</body>
</html>

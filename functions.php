<?php

/**
 * CoopTri child theme text domain
 */
function cooptri_setup() {
    $path = get_stylesheet_directory().'/languages';
    load_child_theme_textdomain( 'go-child', $path );
}
add_action( 'after_setup_theme', 'cooptri_setup' );

/**
 * CoopTri design style
 *
 * @since 0.1.0
 *
 * @param array $default_design_styles Array containings the supported design styles,
 * where the index is the slug of design style and value an array of options that sets up the design styles.
 */
function cooptri_get_available_design_styles( $default_design_styles ) {

    $default_design_styles['brutalist'] = array(
        'slug'           => 'cooptri',
        'label'          => _x( 'CoopTri', 'design style name', 'go' ),
        'url'            => get_stylesheet_directory_uri( __FILE__ ) . '/style.css',
        'editor_style'   => get_stylesheet_directory_uri( __FILE__ ) . '/style.css',
        'color_schemes'  => array(
            'one' => array(
                'label'      => _x( 'Cooptri', 'color palette name', 'go' ),
                'primary'    => '#034561',
                'secondary'  => '#122538',
                'tertiary'   => '#f8f8f8',
                'background' => '#ffffff',
            ),
        ),
        'fonts'          => array(
            'Lato'        => array(
                '300',
                '400',
                '700',
                '900',
            ),
        ),
        'viewport_basis' => '950',
    );

    return $default_design_styles;

}
add_filter( 'go_design_styles', 'cooptri_get_available_design_styles' );

/**
 * Set fixed image size (cropping) for publications:
 */
add_image_size( 'publication-size', 150, 200, true );

/**
 * Limit excerpt to certain amount of words
 *
 * https://smallenvelop.com/limit-post-excerpt-length-in-wordpress/
 */
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);

  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
    if ($limit > 0) $excerpt .= ' ...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

  $readmore = 'Leer más';
  if (ICL_LANGUAGE_CODE == 'en') $readmore = 'Read more'; else if (ICL_LANGUAGE_CODE == 'pt-br') $readmore = 'Ler mais';
  
  $excerpt .= '<a class="read-more" href="'.get_the_permalink().'">' . $readmore . '</a>';

  return $excerpt;
}

/**
 * Limit content to certain amount of words
 */
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);

  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]>', $content);

  $content .= '<a class="read-more" href="'.get_the_permalink().'">' . $readmore . '</a>';

  return $content;
}

/**
 * Add header to contact page
 */
add_action('the_content', 'cooptri_add_content_before_contact');
function cooptri_add_content_before_contact($content) {
    if (stripos($_SERVER['REQUEST_URI'], 'contact') !== false) {

        $before_content = "";

        if (stripos($content, 'form') === false) {
            $before_content = '<p id="breadcrumb"><span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Ir a ." href="https://cooperaciontriangular.org" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> &gt; <span property="itemListElement" typeof="ListItem"><span property="name" class="post-root post post-post current-item">' . __('Contacto', 'go-child') . '</span><meta property="url" content="https://cooperaciontriangular.org/noticias/"><meta property="position" content="2"></span></p><h2>' . __('Contacto', 'go-child') . '</h2>';
        }

        return $before_content . $content;
    }
    else {
        return $content;
    }
}

/**
 * Personalize video gallery template
 * https://plugins360.com/all-in-one-video-gallery/child-themes-and-templates/
 */
function aiovg_custom_videos_template( $tpl )  {
    // $tpl is an absolute path to a file, for example
    // ../public_html/wp-content/plugins/all-in-one-video-gallery/public/templates/videos-template-classic.php
    
    $basename = basename( $tpl );
    // $basename is just a filename for example videos-template-classic.php
     
    if ( 'videos-template-classic.php'  == $basename )  {
        // return path to videos-template-classic.php file in aiovg-custom-templates directory
        return dirname( __FILE__ ) . '/videos-template-classic.php';
    }
    else if ( 'single-video.php'  == $basename )  {
        // return path to videos-template-classic.php file in aiovg-custom-templates directory
        return dirname( __FILE__ ) . '/single-video.php';
    } 
    else {
        return $tpl;
    }
}
add_filter( 'aiovg_load_template', 'aiovg_custom_videos_template' );

/**
 * Video thumbnail HTML output.
 * Only change to the original file is the use of the local directory of the thumbnail template
 */
function cooptri_aiovg_video_thumbnail( $post, $attributes ) {
    // Update Vimeo images to the latest
    $type   = get_post_meta( $post->ID, 'type', true );
    $src    = '';
    $update = 0;    

    if ( 'vimeo' == $type ) {
        $image = get_post_meta( $post->ID, 'image', true );     

        if ( empty( $image ) ) {
            $update = 1;
        } else {
            if ( false !== strpos( $image, 'vimeocdn.com' ) ) {
                $query = parse_url( $image, PHP_URL_QUERY );
                parse_str( $query, $parsed_url );
                
                if ( ! isset( $parsed_url['isnew'] ) ) {                    
                    $update = 1;
                }
            }
        }       

        if ( $update ) {
            $src = get_post_meta( $post->ID, 'vimeo', true );
        }
    }

    if ( 'embedcode' == $type ) {
        $embedcode = get_post_meta( $post->ID, 'embedcode', true );

        $document = new DOMDocument();
        @$document->loadHTML( $embedcode ); 

        $iframes = $document->getElementsByTagName( 'iframe' ); 
        if ( $iframes->length > 0 ) {
            if ( $iframes->item(0)->hasAttribute( 'src' ) ) {
                $src = $iframes->item(0)->getAttribute( 'src' );

                if ( false !== strpos( $src, 'vimeo.com' ) ) {
                    $image = get_post_meta( $post->ID, 'image', true );     

                    if ( empty( $image ) ) {
                        $update = 1;
                    } else {
                        if ( false !== strpos( $image, 'vimeocdn.com' ) ) {
                            $query = parse_url( $image, PHP_URL_QUERY );
                            parse_str( $query, $parsed_url );
                            
                            if ( ! isset( $parsed_url['isnew'] ) ) {                    
                                $update = 1;
                            }
                        }
                    }       
                }
            }
        }       
    }

    if ( $update && $src ) {
        $oembed = aiovg_get_vimeo_oembed_data( $src );
        $thumbnail_url = $oembed['thumbnail_url'];

        update_post_meta( $post->ID, 'image', $thumbnail_url );
    }

    include apply_filters( 'aiovg_load_template', "partials/video-thumbnail.php", $attributes );
}
?>